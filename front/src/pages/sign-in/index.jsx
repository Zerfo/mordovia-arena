import { useState, useEffect } from "react";
import { useDispatch } from "react-redux";

import { push } from "connected-react-router";

import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Link from "@material-ui/core/Link";
import Box from "@material-ui/core/Box";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";

import Alert from "components/Alert";

import { user_login } from "store/user/actions";

import styles from "./style.module.scss";

function SignIn() {
  const dispatch = useDispatch();

  const [isShowAlert, onShowAlert] = useState(false);

  const role = localStorage.getItem("role");

  useEffect(() => {
    !!role && dispatch(push(`/${role}`));
  }, [dispatch, role]);

  const onSubmit = (e) => {
    e.preventDefault();
    const data = {
      [e.target.password.name]: e.target.password.value,
      [e.target.username.name]: e.target.username.value,
    };
    dispatch(user_login(data, () => onShowAlert(true)));
  };

  return (
    <Container component="main" maxWidth="xs" className={styles.container}>
      <div className={styles.top}>
        <Avatar className={styles.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Авторизация
        </Typography>
        <form noValidate onSubmit={onSubmit}>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="username"
            label="Имя пользователя"
            name="username"
            autoComplete="username"
            autoFocus
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Пароль"
            type="password"
            id="password"
            autoComplete="current-password"
          />
          <Button type="submit" fullWidth variant="contained" color="primary">
            Авторизоваться
          </Button>
        </form>
      </div>
      <Box mt={4}>
        <Typography variant="body2" color="textSecondary" align="center">
          {"Copyright © "}
          <Link color="inherit" href="https://porfa.ru" target="_blank">
            Nikolay Smekalin by Porfa
          </Link>{" "}
          {new Date().getFullYear()}
          {"."}
        </Typography>
      </Box>
      {isShowAlert && (
        <Alert
          type="error"
          text="Неверный логин или пароль"
          show={isShowAlert}
          onHide={onShowAlert}
        />
      )}
    </Container>
  );
}

export default SignIn;
