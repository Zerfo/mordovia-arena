/* eslint-disable react-hooks/exhaustive-deps */
import { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { push } from "connected-react-router";

import AppBar from "@material-ui/core/AppBar";
import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";
import InputAdornment from "@material-ui/core/InputAdornment";
import SearchIcon from "@material-ui/icons/Search";
import Tab from "@material-ui/core/Tab";
import Tabs from "@material-ui/core/Tabs";
import TextField from "@material-ui/core/TextField";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";

import Registration from "components/Registration";
import Request from "components/Request";
import Table from "components/Table";
import Alert from "components/Alert";

import { LogoutMain } from "store/main/actions";
import { get_user } from "store/user/actions";
import { get_requests, change_request } from "store/requests/actions";

import {
  isUserSelector,
  userRoleSelector,
  dataTableSelector,
  userIdSelector,
} from "../selectors";

import { SIGN_IN } from "constants/routes";

import styles from "./style.module.scss";

function User() {
  const dispatch = useDispatch();
  const [tab, setTab] = useState(0);

  const isUser = useSelector(isUserSelector);
  const userRole = useSelector(userRoleSelector);
  const userID = useSelector(userIdSelector);

  useEffect(() => {
    !isUser && dispatch(get_user());
    dispatch(get_requests(userRole, userID));
  }, []);

  useEffect(() => {
    dispatch(get_requests(userRole, userID));
  }, [userID, userRole]);

  const handleChangeTab = (event, newTab) => {
    setTab(newTab);
  };
  const logout = () => {
    dispatch(LogoutMain());
    localStorage.removeItem("token");
    localStorage.removeItem("role");
    dispatch(push(SIGN_IN));
  };

  return (
    <div>
      <AppBar position="static">
        <Toolbar className={styles.header}>
          <Tabs
            variant="scrollable"
            scrollButtons="auto"
            value={tab}
            onChange={handleChangeTab}
            centered
          >
            <Tab label="Мои запросы" />
            <Tab label="Создать запрос" />
          </Tabs>
          <Button color="inherit" onClick={logout}>
            Выйти
          </Button>
        </Toolbar>
      </AppBar>
      <TabPanel value={tab} index={0}>
        <UserDataContainer />
      </TabPanel>
      <TabPanel value={tab} index={1}>
        <Request />
      </TabPanel>
      <TabPanel value={tab} index={2}>
        <Registration />
      </TabPanel>
    </div>
  );
}

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

function UserDataContainer() {
  const dispatch = useDispatch();
  const [data, onData] = useState([]);
  const [ids, setIds] = useState([]);
  const [search, onSearch] = useState();
  const [isShowAlert, onShowAlert] = useState(false);
  const [dataAlert, setDataAlert] = useState({});

  const dataTable = useSelector(dataTableSelector);
  const userRole = useSelector(userRoleSelector);
  const userID = useSelector(userIdSelector);

  useEffect(() => {
    onData(
      dataTable.map((i) => ({
        ...i,
        date: `${i.datestart} -- ${i.dateend}`,
      }))
    );
  }, [dataTable]);
  useEffect(() => {
    const searchStr = search?.toUpperCase();
    const data = dataTable.map((i) => ({
      ...i,
      date: `${i.datestart} -- ${i.dateend}`,
    }));

    if (searchStr) {
      const filteredData = data.filter(
        ({ FIO }) => FIO.toUpperCase().indexOf(searchStr) > -1
      );
      onData(filteredData);
    } else {
      onData(data);
    }
  }, [search, dataTable]);

  const getIds = (id) => {
    const data = new Set([...ids, id]);
    setIds([...data]);
  };

  const changeStatuses = () => {
    dispatch(
      change_request(ids, userRole, async (type, text) => {
        await setDataAlert({
          type,
          text,
        });
        onShowAlert(true);
      })
    );
    setTimeout(() => dispatch(get_requests(userRole, userID)), 500);
  };

  return (
    <div className={styles.user__container}>
      <div className={styles.top}>
        <TextField
          id="search"
          label="Поиск"
          variant="outlined"
          onChange={({ target }) => onSearch(target?.value)}
          className={styles.top__textArea}
          startadornment={
            <InputAdornment position="start">
              <SearchIcon />
            </InputAdornment>
          }
        />
        <Button
          className={styles.button}
          variant="contained"
          color="primary"
          onClick={changeStatuses}
        >
          Отменить выделенное
        </Button>
      </div>
      <Table data={data} onChange={getIds} checkboxSelection />
      {isShowAlert && (
        <Alert
          type={dataAlert.type}
          text={dataAlert.text}
          show={isShowAlert}
          onHide={onShowAlert}
        />
      )}
    </div>
  );
}

export default User;
