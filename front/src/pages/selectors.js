export const isUserSelector = (state) => state.user?.account;

export const userRoleSelector = (state) => state.user?.account?.role;

export const dataTableSelector = (state) => state.requests?.data;

export const userSelector = (state) =>
  `${state.user.account?.firstName} ${state.user.account?.lastName}`;

export const userIdSelector = (state) => state.user.account?.id;
