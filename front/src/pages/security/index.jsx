/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { push } from "connected-react-router";

import AppBar from "@material-ui/core/AppBar";
import Button from "@material-ui/core/Button";
import InputAdornment from "@material-ui/core/InputAdornment";
import SearchIcon from "@material-ui/icons/Search";
import TextField from "@material-ui/core/TextField";
import Toolbar from "@material-ui/core/Toolbar";

import Table from "components/Table";

import { LogoutMain } from "store/main/actions";
import { get_user } from "store/user/actions";
import { get_requests } from "store/requests/actions";

import {
  isUserSelector,
  userRoleSelector,
  dataTableSelector,
} from "../selectors";

import { SIGN_IN } from "constants/routes";

import styles from "./style.module.scss";

function Security() {
  const dispatch = useDispatch();
  const isUser = useSelector(isUserSelector);
  const userRole = useSelector(userRoleSelector);
  const dataTable = useSelector(dataTableSelector);

  const [data, onData] = useState([]);
  const [search, onSearch] = useState();

  useEffect(() => {
    !isUser && dispatch(get_user());
    dispatch(get_requests(userRole));
    const interval = setInterval(() => {
      dispatch(get_requests(userRole));
    }, 60000);
    return () => clearInterval(interval);
  }, []);

  useEffect(() => {
    onData(
      dataTable.map((i) => ({
        ...i,
        date: `${i.datestart} -- ${i.dateend}`,
      }))
    );
  }, [dataTable]);
  useEffect(() => {
    const searchStr = search?.toUpperCase();
    const data = dataTable.map((i) => ({
      ...i,
      date: `${i.datestart} -- ${i.dateend}`,
    }));

    if (searchStr) {
      const filteredData = data.filter(
        ({ FIO }) => FIO.toUpperCase().indexOf(searchStr) > -1
      );
      onData(filteredData);
    } else {
      onData(data);
    }
  }, [search, dataTable]);

  useEffect(() => {
    dispatch(get_requests(userRole));
  }, [userRole, isUser]);

  const logout = () => {
    dispatch(LogoutMain());
    localStorage.removeItem("token");
    localStorage.removeItem("role");
    dispatch(push(SIGN_IN));
  };

  return (
    <>
      <AppBar position="static">
        <Toolbar className={styles.header}>
          <Button color="inherit" onClick={logout}>
            Выйти
          </Button>
        </Toolbar>
      </AppBar>
      <div className={styles.container}>
        <div className={styles.top}>
          <TextField
            id="search"
            label="Поиск"
            variant="outlined"
            onChange={({ target }) => onSearch(target?.value)}
            className={styles.top__textArea}
            startadornment={
              <InputAdornment position="start">
                <SearchIcon />
              </InputAdornment>
            }
          />
        </div>
        <Table data={data} />
      </div>
    </>
  );
}

export default Security;
