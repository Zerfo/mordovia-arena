import { Suspense, lazy } from "react";

import { Route, Switch, Redirect } from "react-router";
import { ConnectedRouter } from "connected-react-router";
import { useSelector } from "react-redux";

import Loader from "components/Loader";

import { ADMIN, USER, SECURITY, SIGN_IN } from "constants/routes";

import { selectorIsPending } from "./selectors";

const Admin = lazy(() => import("pages/admin"));
const Security = lazy(() => import("pages/security"));
const User = lazy(() => import("pages/user"));
const SignIn = lazy(() => import("pages/sign-in"));

function App({ history }) {
  const isPending = useSelector(selectorIsPending);

  return (
    <ConnectedRouter history={history}>
      <Suspense fallback={<Loader />}>
        <Switch>
          <Route path={SIGN_IN} component={SignIn} />
          <Route path={ADMIN} component={Admin} />
          <Route path={SECURITY} component={Security} />
          <Route path={USER} component={User} />
        </Switch>
        <Redirect to={SIGN_IN} />
      </Suspense>
      {isPending && <Loader />}
    </ConnectedRouter>
  );
}

export default App;
