export const selectorIsLogin = (state) => state.main?.isLogin;

export const selectorIsPending = (state) => state.main?.isPending;
