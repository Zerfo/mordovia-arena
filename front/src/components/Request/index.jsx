import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";

import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Link from "@material-ui/core/Link";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
import ScreenShareIcon from "@material-ui/icons/ScreenShare";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import { useDropzone } from "react-dropzone";

import Alert from "components/Alert";

import {
  get_requests,
  add_request,
  file_request,
} from "store/requests/actions";

import {
  userRoleSelector,
  userSelector,
  userIdSelector,
} from "pages/selectors";

import styles from "./style.module.scss";

import Template from "./TEMPLATE.xlsx";
import XLSX from "./xlsx.svg";
import trash from "./trash.svg";

function Request() {
  const dispatch = useDispatch();
  const userRole = useSelector(userRoleSelector);
  const username = useSelector(userSelector);
  const userId = useSelector(userIdSelector);

  const { getRootProps, getInputProps } = useDropzone({
    accept: ".xlsx",
    onDrop: (acceptedFile) => setFile(acceptedFile[0]),
  });

  const [isShowAlert, onShowAlert] = useState(false);
  const [dataAlert, setDataAlert] = useState({});
  const [file, setFile] = useState(null);

  const onSubmit = (e) => {
    e.preventDefault();
    if (!!file) {
      const data = {
        file,
        username,
        userId,
      };
      dispatch(
        file_request(data, userRole, async (type, text) => {
          await setDataAlert({
            type,
            text,
          });
          onShowAlert(true);
        })
      );
    } else {
      const data = {
        user: username,
        userID: userId,
        [e.target.fio.name]: e.target.fio.value,
        [e.target.datestart.name]: e.target.datestart.value,
        [e.target.dateend.name]: e.target.dateend.value,
        [e.target.purpose.name]: e.target.purpose.value,
      };
      dispatch(
        add_request(data, userRole, async (type, text) => {
          await setDataAlert({
            type,
            text,
          });
          onShowAlert(true);
        })
      );
    }
    e.target.purpose.value = null;
    e.target.fio.value = null;
    e.target.datestart.value = null;
    e.target.dateend.value = null;
    e.target.date.value = null;

    setFile(null);
    setTimeout(() => dispatch(get_requests(userRole)), 1000);
  };

  return (
    <Container component="main" maxWidth="xs" className={styles.container}>
      <div className={styles.top}>
        <Avatar className={styles.avatar}>
          <ScreenShareIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Создать запрос
        </Typography>
        <form onSubmit={onSubmit} noValidate className={styles.form}>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                name="FIO"
                label="ФИО"
                type="text"
                id="fio"
                autoComplete="current-fio"
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                name="datestart"
                label="Дата начала"
                type="date"
                id="date"
                autoComplete="current-date"
                defaultValue={new Date().toDateString()}
                InputLabelProps={{
                  shrink: true,
                }}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                name="dateend"
                label="Дата окончания"
                type="date"
                id="date"
                autoComplete="current-date"
                defaultValue={new Date().toDateString()}
                InputLabelProps={{
                  shrink: true,
                }}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                name="purpose"
                label="Причина"
                type="text"
                id="purpose"
                autoComplete="current-purpose"
              />
            </Grid>
          </Grid>
          <div className={styles.fileUploader}>
            <p className={styles.title}>Или загрузите файл</p>
            <a download="TEMPLATE.xlsx" href={Template}>
              Скачать шаблон
            </a>
            <div {...getRootProps({ className: styles.dropzone })}>
              <input {...getInputProps()} />
              <p>Перетащите необходимый файл или кликните для его выбора</p>
            </div>
            <aside className={styles.thumbsContainer}>
              {!!file ? (
                <div className={styles.thumb} key={file.name}>
                  <div>
                    <img src={XLSX} className={styles.img} alt="" />
                    <p>{file.name}</p>
                  </div>
                  <div
                    className={styles.img__trash}
                    onClick={() => setFile(null)}
                  >
                    <img src={trash} alt="" />
                  </div>
                </div>
              ) : null}
            </aside>
          </div>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={styles.btn}
          >
            Отправить
          </Button>
        </form>
      </div>
      <Box mt={4}>
        <Typography variant="body2" color="textSecondary" align="center">
          {"Copyright © "}
          <Link color="inherit" href="https://porfa.ru" target="_blank">
            Nikolay Smekalin by Porfa
          </Link>{" "}
          {new Date().getFullYear()}
          {"."}
        </Typography>
      </Box>
      {isShowAlert && (
        <Alert
          type={dataAlert.type}
          text={dataAlert.text}
          show={isShowAlert}
          onHide={onShowAlert}
        />
      )}
    </Container>
  );
}

export default Request;
