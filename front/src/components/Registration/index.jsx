import { useState } from "react";
import { useDispatch } from "react-redux";

import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Link from "@material-ui/core/Link";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";

import Alert from "components/Alert";

import { registration } from "store/registration/actions";

import styles from "./style.module.scss";

function Registration() {
  const dispatch = useDispatch();
  const [role, setRole] = useState(0);
  const [isShowAlert, onShowAlert] = useState(false);
  const [dataAlert, setDataAlert] = useState({});

  const handleChangeRole = (event, newRole) => {
    setRole(newRole);
  };
  const onSubmit = (e) => {
    e.preventDefault();
    const data = {
      [e.target.firstName.name]: e.target.firstName.value,
      [e.target.lastName.name]: e.target.lastName.value,
      [e.target.username.name]: e.target.username.value,
      [e.target.password.name]: e.target.password.value,
      [e.target.role && "role"]: e.target.role.value,
    };
    dispatch(
      registration(data, async (type, text) => {
        await setDataAlert({
          type,
          text,
        });
        onShowAlert(true);
        resetForm(e);
      })
    );
  };
  const resetForm = (e) => {
    e.target.firstName.value = null;
    e.target.lastName.value = null;
    e.target.username.value = null;
    e.target.password.value = null;
    e.target.role.value = null;
  };

  return (
    <Container component="main" maxWidth="xs" className={styles.container}>
      <div className={styles.top}>
        <Avatar className={styles.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Регистрация
        </Typography>
        <form onSubmit={onSubmit} className={styles.form} noValidate>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <TextField
                autoComplete="fname"
                name="firstName"
                variant="outlined"
                required
                fullWidth
                id="firstName"
                label="Имя"
                autoFocus
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="lastName"
                label="Фамилия"
                name="lastName"
                autoComplete="lname"
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="username"
                label="Имя пользователя"
                name="username"
                autoComplete="username"
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                name="password"
                label="Пароль"
                type="password"
                id="password"
                autoComplete="current-password"
              />
            </Grid>
            <Grid item xs={12}>
              <RadioGroup
                aria-label="role"
                name="role"
                value={role}
                onChange={handleChangeRole}
                className={styles.role}
              >
                <FormControlLabel
                  value="user"
                  name="role"
                  control={<Radio />}
                  label="Пользователь"
                />
                <FormControlLabel
                  value="security"
                  name="role"
                  control={<Radio />}
                  label="Охрана"
                />
                <FormControlLabel
                  value="admin"
                  name="role"
                  control={<Radio />}
                  label="Администратор"
                />
              </RadioGroup>
            </Grid>
          </Grid>
          <Button type="submit" fullWidth variant="contained" color="primary">
            Зарегестрировать
          </Button>
        </form>
      </div>
      <Box mt={4}>
        <Typography variant="body2" color="textSecondary" align="center">
          {"Copyright © "}
          <Link color="inherit" href="https://porfa.ru" target="_blank">
            Nikolay Smekalin by Porfa
          </Link>{" "}
          {new Date().getFullYear()}
          {"."}
        </Typography>
      </Box>
      {isShowAlert && (
        <Alert
          type={dataAlert.type}
          text={dataAlert.text}
          show={isShowAlert}
          onHide={onShowAlert}
        />
      )}
    </Container>
  );
}

export default Registration;
