import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from "@material-ui/lab/Alert";

function Alert({ type, text, show, onHide }) {
  return (
    <Snackbar
      open={show}
      autoHideDuration={3000}
      anchorOrigin={{
        vertical: "bottom",
        horizontal: "right",
      }}
      onClose={() => onHide(false)}
    >
      <MuiAlert severity={type}>{text}</MuiAlert>
    </Snackbar>
  );
}

export default Alert;
