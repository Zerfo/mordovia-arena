import { DataGrid } from "@material-ui/data-grid";

import styles from "./styles.module.scss";

const TABLE_COLUMNS = [
  { field: "FIO", headerName: "ФИО", width: 300 },
  { field: "purpose", headerName: "Причина", width: 500 },
  { field: "date", headerName: "Дата начала", width: 200 },
  { field: "user", headerName: "Запрашивающий", width: 250 },
  { field: "approve", headerName: "Статус", width: 200 },
  { field: "id", headerName: "ID", width: 70 },
];

function Table({ data, checkboxSelection, onChange }) {
  return (
    <div className={styles.table}>
      <DataGrid
        onCellClick={(params) => onChange?.(params.row?.id)}
        rows={data}
        columns={TABLE_COLUMNS}
        pageSize={100}
        GridSlotsComponent={{ NoRowsOverlay: "Нет заявок" }}
        checkboxSelection={checkboxSelection}
      />
    </div>
  );
}

export default Table;
