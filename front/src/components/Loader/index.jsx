import CircularProgress from "@material-ui/core/CircularProgress";

import style from "./style.module.scss";

function Loader() {
  return (
    <>
      <div className={style.overlay} />
      <div className={style.loader}>
        <CircularProgress />
      </div>
    </>
  );
}

export default Loader;
