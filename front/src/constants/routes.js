export const DEFAULT_ROUTE = "/";
export const SIGN_IN = `${DEFAULT_ROUTE}sign-in`;
export const ADMIN = `${DEFAULT_ROUTE}admin`;
export const USER = `${DEFAULT_ROUTE}user`;
export const SECURITY = `${DEFAULT_ROUTE}security`;
export const NOT_MATCH = `${DEFAULT_ROUTE}404`;
