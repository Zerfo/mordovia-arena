import ax from "axios";

export function sendRequest(method, url, body = {}, header = {}, contentType) {
  const config = {
    method, // *GET, POST, PUT, DELETE, etc.
    mode: "cors", // no-cors, *cors, same-origin
    cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
    credentials: "include", // include, *same-origin, omit
    headers: {
      ...header,
      "Content-Type": contentType || "application/json; charset=UTF-8",
    },
    redirect: "follow", // manual, *follow, error
    referrerPolicy: "no-referrer", // no-referrer, *clientr
  };

  if (Object.keys(body).length >= 1) {
    config.data = JSON.stringify(body);
  }

  return ax(url, config)
    .then((res) => {
      return res;
    })
    .catch((err) => {
      throw err.response;
    });
}

export default sendRequest;
