import STORE_CONSTANTS from "store/config/contants";

const MAIN_CONSTANTS = STORE_CONSTANTS.MAIN;

export const pendingStart = () => ({
  type: MAIN_CONSTANTS.PENDING_START,
});
export const pendingEnd = () => ({
  type: MAIN_CONSTANTS.PENDING_END,
});

export const LoginMain = () => ({
  type: MAIN_CONSTANTS.LOGIN,
});
export const LogoutMain = () => ({
  type: MAIN_CONSTANTS.LOGOUT,
});
