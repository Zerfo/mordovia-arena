import STORE_CONSTANTS from "store/config/contants";

const MAIN_CONSTANTS = STORE_CONSTANTS.MAIN;

const initialState = {
  isPending: false,
  isLogin: true,
};

export default function mainReducer(state = initialState, action) {
  switch (action.type) {
    case MAIN_CONSTANTS.PENDING_START:
      return {
        ...state,
        isPending: true,
      };
    case MAIN_CONSTANTS.PENDING_END:
      return {
        ...state,
        isPending: false,
      };
    case MAIN_CONSTANTS.LOGIN:
      return {
        ...state,
        isLogin: true,
      };
    case MAIN_CONSTANTS.LOGOUT:
      localStorage.removeItem("token");
      localStorage.removeItem("role");
      return {
        ...state,
        isLogin: false,
      };
    default:
      return state;
  }
}
