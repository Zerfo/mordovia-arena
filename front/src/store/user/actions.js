import STORE_CONSTANTS from "store/config/contants";

const USER_CONSTANTS = STORE_CONSTANTS.USER;

export const user_login = (data, onResult) => ({
  type: USER_CONSTANTS.USER_LOGIN_START,
  data,
  onResult,
});

export const user_login_ok = (data) => ({
  type: USER_CONSTANTS.USER_LOGIN_OK,
  data,
});

export const user_login_fail = (err) => ({
  type: USER_CONSTANTS.USER_LOGIN_FAIL,
  err,
});

export const get_user = () => ({
  type: USER_CONSTANTS.GET_USER_START,
  token: localStorage.getItem("token"),
});

export const get_user_ok = (data) => ({
  type: USER_CONSTANTS.GET_USER_OK,
  data,
});

export const get_user_fail = (err) => ({
  type: USER_CONSTANTS.GET_USER_FAIL,
  err,
});
