import STORE_CONSTANTS from "store/config/contants";

const USER_CONSTANTS = STORE_CONSTANTS.USER;

const initialState = {
  account: null,
  error: null,
};

export default function userReducer(state = initialState, action) {
  switch (action.type) {
    case USER_CONSTANTS.USER_LOGIN_OK:
    case USER_CONSTANTS.GET_USER_OK:
      return {
        ...state,
        account: action.data,
      };
    case USER_CONSTANTS.USER_LOGIN_FAIL:
    case USER_CONSTANTS.GET_USER_FAIL:
      return {
        ...state,
        account: null,
        error: action.err,
      };
    default:
      return state;
  }
}
