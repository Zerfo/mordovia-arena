import { call, put, takeLatest, delay } from "redux-saga/effects";
import { push } from "connected-react-router";

import req from "api/request";

import { pendingStart, pendingEnd } from "store/main/actions";
import {
  user_login_ok,
  user_login_fail,
  get_user_ok,
  get_user_fail,
} from "store/user/actions";

import STORE_CONSTANTS from "store/config/contants";
import { SIGN_IN } from "constants/routes";
import { REQUEST_URL } from "utils/constants";

const USER_CONSTANTS = STORE_CONSTANTS.USER;

function* sagaLogin({ data, onResult }) {
  try {
    yield put(pendingStart());
    const URL = `${REQUEST_URL}/api/login`;
    const res = yield call(req, "POST", URL, data);
    const userData = {
      ...res?.data?.data,
      ...res?.data?.data?.attributes,
    };
    yield put(user_login_ok(userData));
    localStorage.setItem("token", res?.data?.data?.refreshToken);
    localStorage.setItem("role", res?.data?.data?.role);
    const role = res?.data?.data?.role;
    yield delay(1000);
    yield put(pendingEnd());
    yield put(push(`/${role}`));
  } catch (error) {
    onResult();
    yield put(user_login_fail(error));
    yield delay(1000);
    yield put(pendingEnd());
    localStorage.removeItem("token");
    localStorage.removeItem("role");
    yield put(push(SIGN_IN));
  }
}

function* sagaGetUser({ token }) {
  try {
    yield put(pendingStart());
    const URL = `${REQUEST_URL}/api/refresh`;
    const res = yield call(req, "POST", URL, { token });
    const data = {
      ...res?.data?.data,
      ...res?.data?.data?.attributes,
    };
    const role = res?.data?.data?.attributes?.role;
    localStorage.setItem("token", res?.data?.data?.refreshToken);
    localStorage.setItem("role", res?.data?.data?.attributes?.role);
    yield put(get_user_ok(data));
    yield put(push(`/${role}`));
    yield delay(2000);
    yield put(pendingEnd());
  } catch (error) {
    yield put(get_user_fail(error));
    yield delay(2000);
    localStorage.removeItem("token");
    localStorage.removeItem("role");
    yield put(pendingEnd());
    yield put(push(SIGN_IN));
  }
}

export default function* userWatcher() {
  yield takeLatest(USER_CONSTANTS.USER_LOGIN_START, sagaLogin);
  yield takeLatest(USER_CONSTANTS.GET_USER_START, sagaGetUser);
}
