import STORE_CONSTANTS from "store/config/contants";

const REG_CONSTANTS = STORE_CONSTANTS.REGISTRATION;

export const registration = (data, onResult) => ({
  type: REG_CONSTANTS.REG_START,
  data,
  onResult,
});

export const registration_ok = (data) => ({
  type: REG_CONSTANTS.REG_OK,
  data,
});

export const registration_fail = (error) => ({
  type: REG_CONSTANTS.REG_FAIL,
  error,
});
