import { call, put, takeLatest, delay } from "redux-saga/effects";

import req from "api/request";

import { pendingStart, pendingEnd } from "store/main/actions";

import STORE_CONSTANTS from "store/config/contants";
import { REQUEST_URL } from "utils/constants";

const REG_CONSTANTS = STORE_CONSTANTS.REGISTRATION;

function* sagaRegistration({ data, onResult }) {
  try {
    yield put(pendingStart());
    const URL = `${REQUEST_URL}/api/singup`;
    yield call(req, "POST", URL, data);
    onResult("success", "Регистрация прошла успешно.");
    yield delay(2000);
    yield put(pendingEnd());
  } catch (error) {
    onResult(
      "error",
      "Внутренняя ошибка системы. Обратитесь к системному администратору."
    );
    yield delay(2000);
    yield put(pendingEnd());
  }
}

export default function* registrationWatcher() {
  yield takeLatest(REG_CONSTANTS.REG_START, sagaRegistration);
}
