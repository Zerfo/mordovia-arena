import STORE_CONSTANTS from "store/config/contants";

const REQ_CONSTANTS = STORE_CONSTANTS.REQUESTS;

const initialState = {
  data: [],
  error: null,
};

export default function userReducer(state = initialState, action) {
  switch (action.type) {
    case REQ_CONSTANTS.GET_REQUESTS_OK:
      return {
        ...state,
        data: action.data,
      };
    case REQ_CONSTANTS.GET_REQUESTS_FAIL:
      return {
        ...state,
        data: [],
        error: action.err,
      };
    default:
      return state;
  }
}
