import { call, put, takeLatest, delay } from "redux-saga/effects";
import { push } from "connected-react-router";
import ax from "axios";

import req from "api/request";

import { pendingStart, pendingEnd } from "store/main/actions";

import {
  get_requests_ok,
  get_requests_fail,
  add_request_ok,
  add_request_fail,
  change_request_ok,
  change_request_fail,
} from "store/requests/actions";

import STORE_CONSTANTS from "store/config/contants";

import { SIGN_IN } from "constants/routes";
import { REQUEST_URL } from "utils/constants";

const REQ_CONSTANTS = STORE_CONSTANTS.REQUESTS;

function* sagaGetRequests({ role, userID, onResult }) {
  if (role) {
    try {
      yield put(pendingStart());
      const URL = `${REQUEST_URL}/api/${role}/requests`;
      let res;
      if (userID) {
        res = yield call(req, "GET", `${URL}/?userID=${userID}`);
      } else {
        res = yield call(req, "GET", URL);
      }
      const data = res.data.data.map((i) => ({
        ...i,
        approve: i.approve ? "Одобрено" : "На рассмотрении или отклонено",
      }));
      yield put(get_requests_ok(data));
      yield put(pendingEnd());
    } catch (err) {
      yield put(get_requests_fail(err));
      yield delay(1000);
      yield put(pendingEnd());
      yield put(push(SIGN_IN));
    }
  }
}

function* sagaAddRequest({ data, role, onResult }) {
  try {
    yield put(pendingStart());
    const URL = `${REQUEST_URL}/api/${role}/add-request`;
    const res = yield call(req, "POST", URL, data);
    onResult("success", "Запрос отправлен.");
    yield put(add_request_ok(res.data.data));
    yield put(pendingEnd());
  } catch (err) {
    onResult(
      "error",
      "Внутренняя ошибка системы. Обратитесь к системному администратору."
    );
    yield put(add_request_fail(err));
    yield delay(1000);
    yield put(pendingEnd());
  }
}

function* sagaFileRequest({ data, role, onResult }) {
  try {
    yield put(pendingStart());
    const URL = `${REQUEST_URL}/api/${role}/file-requests`;
    const formData = new FormData();
    formData.append("file", data.file);
    formData.append("user", data.username);
    formData.append("userId", data.userId);
    const res = yield ax.post(URL, formData, {
      headers: {
        "Content-Type": "multipart/form-data",
      },
    });
    // const res = yield call(ax, "POST", URL, data, null, "multipart/form-data;");
    onResult("success", "Запрос отправлен.");
    yield put(add_request_ok(res.data.data));
    yield put(pendingEnd());
  } catch (err) {
    onResult(
      "error",
      "Внутренняя ошибка системы. Обратитесь к системному администратору."
    );
    yield put(add_request_fail(err));
    yield delay(1000);
    yield put(pendingEnd());
  }
}

function* sagaChangeRequests({ data, role, onResult }) {
  try {
    yield put(pendingStart());
    const URL = `${REQUEST_URL}/api/${role}/change-status`;
    const res = yield call(req, "POST", URL, data);
    onResult("success", "Статус успешно изменен.");
    yield put(change_request_ok(res.data.data));
    yield put(pendingEnd());
  } catch (err) {
    onResult(
      "error",
      "Внутренняя ошибка системы. Обратитесь к системному администратору."
    );
    yield put(change_request_fail(err));
    yield delay(1000);
    yield put(pendingEnd());
  }
}

export default function* userWatcher() {
  yield takeLatest(REQ_CONSTANTS.GET_REQUESTS_START, sagaGetRequests);
  yield takeLatest(REQ_CONSTANTS.POST_REQUEST_START, sagaAddRequest);
  yield takeLatest(
    REQ_CONSTANTS.CHANGE_REQUEST_STATUS_START,
    sagaChangeRequests
  );
  yield takeLatest(REQ_CONSTANTS.POST_FILE_REQUEST_START, sagaFileRequest);
}
