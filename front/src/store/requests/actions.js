import STORE_CONSTANTS from "store/config/contants";

const REQ_CONSTANTS = STORE_CONSTANTS.REQUESTS;

export const get_requests = (role, userID, onResult) => ({
  type: REQ_CONSTANTS.GET_REQUESTS_START,
  onResult,
  role,
  userID,
});
export const get_requests_ok = (data) => ({
  type: REQ_CONSTANTS.GET_REQUESTS_OK,
  data,
});
export const get_requests_fail = (err) => ({
  type: REQ_CONSTANTS.GET_REQUESTS_FAIL,
  err,
});

export const add_request = (data, role, onResult) => ({
  type: REQ_CONSTANTS.POST_REQUEST_START,
  data,
  role,
  onResult,
});
export const add_request_ok = () => ({
  type: REQ_CONSTANTS.POST_REQUEST_OK,
});
export const add_request_fail = (err) => ({
  type: REQ_CONSTANTS.POST_REQUEST_FAIL,
  err,
});

export const file_request = (data, role, onResult) => ({
  type: REQ_CONSTANTS.POST_FILE_REQUEST_START,
  data,
  role,
  onResult,
});
export const file_request_ok = () => ({
  type: REQ_CONSTANTS.POST_FILE_REQUEST_OK,
});
export const file_request_fail = (err) => ({
  type: REQ_CONSTANTS.POST_FILE_REQUEST_FAIL,
  err,
});

export const change_request = (data, role, onResult) => ({
  type: REQ_CONSTANTS.CHANGE_REQUEST_STATUS_START,
  data,
  role,
  onResult,
});
export const change_request_ok = () => ({
  type: REQ_CONSTANTS.CHANGE_REQUEST_STATUS_OK,
});
export const change_request_fail = (err) => ({
  type: REQ_CONSTANTS.CHANGE_REQUEST_STATUS_FAIL,
  err,
});
