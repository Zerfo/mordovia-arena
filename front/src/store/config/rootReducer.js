import { combineReducers } from "redux";
import { connectRouter } from "connected-react-router";

import main from "store/main/reducer";
import user from "store/user/reducer";
import requests from "store/requests/reducer";

const rootReducer = (history) =>
  combineReducers({
    main,
    requests,
    router: connectRouter(history),
    user,
    //
  });

export default rootReducer;
