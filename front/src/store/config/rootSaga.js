import { all } from "redux-saga/effects";

import registrationWatcher from "store/registration/sagas";
import userWatcher from "store/user/sagas";
import requestsWatcher from "store/requests/sagas";

export default function* rootSaga() {
  yield all([
    registrationWatcher(),
    userWatcher(),
    requestsWatcher(),
    //
  ]);
}
