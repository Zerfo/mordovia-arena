import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";

import App from "containers/App";

import configureStore from "store";
import history from "store/config/history";

import reportWebVitals from "./reportWebVitals";

import "styles/index.scss";

const rootEl = document.getElementById("root");
const store = configureStore();

const render = () =>
  ReactDOM.render(
    <React.StrictMode>
      <Provider store={store}>
        <App history={history} />
      </Provider>
    </React.StrictMode>,
    rootEl
  );

render();

reportWebVitals();
