const express = require("express");
const router = express.Router();
const { compareSync } = require("bcrypt");
const jwt = require("jsonwebtoken");
const { v4: uuid } = require("uuid");
const { hashSync } = require("bcrypt");

const User = require("../../../database/schemas/user");

router.post("/singup", async (req, res) => {
  try {
    console.log(1);
    const { role, username, firstName, lastName, password } = req.body;
    console.log(req.body);
    await User.sync();
    console.log(2);
    const newUser = await User.create({
      role,
      username,
      firstName,
      lastName,
      password: hashSync(password, 10),
    });
    console.log(newUser);
    res.status(200).send({
      status: "Ok",
      code: 200,
      id: newUser.id,
      role: newUser.role,
      username: newUser.username,
      firstName: newUser.firstName,
      lastName: newUser.lastName,
    });
  } catch (error) {
    res.status(401).send({
      status: "error",
      code: 401,
      error,
    });
  }
});

router.post("/login", async (req, res) => {
  const { username, password } = req.body;
  await User.sync();
  const user = await User.findOne({ where: { username: username } });
  if (
    !user ||
    !username ||
    !password ||
    !compareSync(password, user.password)
  ) {
    return res.status(404).send({
      status: "error",
      code: 404,
      message: "user wasn't found",
    });
  }
  const refreshToken = uuid();
  await user.updateAttributes({ refreshToken: refreshToken });
  return res.status(200).send({
    status: "Ok",
    code: "200",
    data: {
      accessToken: jwt.sign({ id: user.id, role: user.role }, "SECRET"),
      refreshToken: refreshToken,
      type: "profile",
      role: user.role,
      attributes: {
        firstName: user.firstName,
        id: user.id,
        lastName: user.lastName,
        username: user.username,
        userRole: user.role,
      },
    },
  });
});

router.post("/refresh", async (req, res) => {
  const { token } = req.body;
  const user = await User.findOne({ where: { refreshToken: token } });

  if (!user || !req.body.token)
    return res.status(404).send({
      status: "error",
      code: 404,
      message: "Refresh token was not found",
    });
  const newRefreshToken = uuid();

  await user.update({ ...user, refreshToken: newRefreshToken });
  return res.status(200).send({
    status: "Ok",
    code: 200,
    data: {
      accessToken: jwt.sign({ id: user.id, role: user.role }, "SECRET"),
      refreshToken: newRefreshToken,
      type: "profile",
      attributes: {
        firstName: user.firstName,
        id: user.id,
        lastName: user.lastName,
        role: user.role,
        username: user.username,
        username: user.username,
        userRole: user.role,
      },
    },
  });
});

module.exports = router;
