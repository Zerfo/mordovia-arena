const express = require("express");
const router = express.Router();
const Sequelize = require("sequelize");
const moment = require("moment");

const Requests = require("../../../database/schemas/requests");

const Op = Sequelize.Op;

router.get("/requests", async (req, res) => {
  try {
    const date = moment().format("YYYY-MM-DD");
    await Requests.sync();
    const data = await Requests.findAll({
      where: {
        [Op.or]: [
          {
            datestart: {
              [Op.lte]: date,
            },
            dateend: {
              [Op.gte]: date,
            },
          },
          { date },
        ],
        [Op.and]: { approve: true },
      },
      order: ["FIO"],
    });

    res.status(200).send({
      status: "Ok",
      code: 200,
      data: data,
    });
  } catch (err) {
    res.status(401).send({
      status: "error",
      code: 401,
      // type: errors.type,
      // message: errors.message,
    });
  }
});

module.exports = router;
