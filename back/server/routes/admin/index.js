const express = require("express");
const router = express.Router();
const multer = require("multer");
const xlsxFile = require("read-excel-file/node");
const moment = require("moment");

const storageConfig = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "uploads");
  },
  filename: (req, file, cb) => {
    cb(null, file.originalname);
  },
});

const Requests = require("../../../database/schemas/requests");

router.get("/requests", async (req, res) => {
  try {
    await Requests.sync();
    const data = await Requests.findAll({ order: [["createdAt"]] });
    res.status(200).send({
      status: "Ok",
      code: 200,
      data: data.reverse(),
    });
  } catch ({ errors }) {
    res.status(401).send({
      status: "error",
      code: 401,
      // type: errors.type,
      // message: errors.message,
    });
  }
});

router.post("/change-status", async (req, res) => {
  try {
    await Requests.sync();
    req.body.forEach(async (itm) => {
      const request = await Requests.findOne({ where: { id: itm } });
      request.update({
        approve: true,
      });
    });
  } catch (e) {
    res.status(401).send({
      status: "error",
      code: 401,
      err: e,
      // type: errors.type,
      // message: errors.message,
    });
  }
});

router.post("/add-request", async (req, res) => {
  try {
    const { FIO, purpose, user, userID, date, datestart, dateend } = req.body;
    const dt = moment().format("YYYY-MM-DD");
    await Requests.sync();
    const newRequest = await Requests.create({
      FIO,
      purpose,
      user,
      userID,
      date,
      approve: true,
      datestart,
      dateend,
    });
    res.status(200).send({
      status: "Ok",
      code: 200,
      FIO: newRequest.FIO,
      purpose: newRequest.purpose,
      user: newRequest.user,
      userID: newRequest.userID,
      date: newRequest.date || dt,
      approve: newRequest.approve,
      datestart: newRequest.datestart,
      dateend: newRequest.dateend,
    });
  } catch (e) {
    res.status(401).send({
      status: "error",
      code: 401,
      error: JSON.stringify(e)
      // type: errors.type,
      // message: errors.message,
    });
  }
});

router.post(
  "/file-requests",
  multer({ storage: storageConfig }).single("file"),
  (req, res) => {
    try {
      const { user, userId } = req.body;
      xlsxFile(process.cwd() + "/uploads/" + req.file.filename).then(
        async (rows) => {
          delete rows[0];
          const data = rows.filter((i) => i !== null);
          const dataObjs = data.map((i) => ({
            datestart: i[2],
            dateend: i[3],
            FIO: i[0],
            purpose: i[1],
            user,
            userID: +userId,
            approve: true,
          }));

          await Requests.sync();
          dataObjs.forEach(
            async (i) =>
              await Requests.create({
                FIO: i.FIO,
                purpose: i.purpose,
                user: i.user,
                userID: i.userID,
                datestart: i.datestart,
                dateend: i.dateend,
                approve: i.approve,
              })
          );
          res.status(200).send({
            status: "Ok",
            code: 200,
            dataObjs,
          });
        }
      );
    } catch (e) {
      res.status(401).send({
        status: "error",
        code: 401,
      });
    }
  }
);

module.exports = router;
