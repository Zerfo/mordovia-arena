const express = require("express");
const cookieParser = require("cookie-parser");
const methodOverride = require("method-override");
const cors = require("cors");

const authModule = require("./routes/auth");
const adminModule = require("./routes/admin");
const userModule = require("./routes/user");
const securityModule = require("./routes/security");

const app = express();
const PORT = process.env.PORT || 5000;

const allowCrossDomain = function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,OPTIONS");
  res.header(
    "Access-Control-Allow-Headers",
    "Content-Type, Authorization, Content-Length, X-Requested-With"
  );

  // intercept OPTIONS method
  if ("OPTIONS" == req.method) {
    res.send(200);
  } else {
    next();
  }
};
app.use(cors());
app.use(allowCrossDomain);
app.use(express.json());
app.use(
  express.urlencoded({
    extended: true,
  })
);

app.use("/api", authModule);
app.use("/api/admin", adminModule);
app.use("/api/user", userModule);
app.use("/api/security", securityModule);

app.listen(PORT, () => {
  console.log(`Server started on localhost:${PORT}`);
  require("../database");
});
