const Sequelize = require("sequelize");
const sequelize = require("../");

module.exports = sequelize.define("requests", {
  FIO: {
    type: Sequelize.TEXT,
    allowNull: false,
    validate: {
      len: [4, 200],
    },
  },
  approve: {
    type: Sequelize.BOOLEAN,
    allowNull: false,
    defaultValue: false,
  },
  purpose: {
    type: Sequelize.TEXT,
    allowNull: false,
    validate: {
      len: [4, 1000],
    },
  },
  datestart: {
    type: Sequelize.DATEONLY,
    allowNull: true,
  },
  dateend: {
    type: Sequelize.DATEONLY,
    allowNull: true,
  },
  user: {
    type: Sequelize.TEXT,
    allowNull: false,
    validate: {
      len: [4, 1000],
    },
  },
  userID: {
    type: Sequelize.INTEGER,
    allowNull: false,
  },
  date: {
    type: Sequelize.DATEONLY,
  },
});
