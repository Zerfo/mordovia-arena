const Sequelize = require("sequelize");
const sequelize = require("../");

module.exports = sequelize.define("user", {
  role: {
    type: Sequelize.TEXT,
    allowNull: false,
    unique: false,
    validate: {
      len: [4, 20],
    },
  },
  username: {
    type: Sequelize.TEXT,
    allowNull: false,
    unique: true,
    validate: {
      len: [4, 200],
    },
  },
  password: {
    type: Sequelize.TEXT,
    allowNull: false,
    unique: false,
    validate: {
      len: [8, 999],
    },
  },
  firstName: {
    type: Sequelize.TEXT,
    allowNull: false,
    unique: false,
    validate: {
      len: [4, 200],
    },
  },
  lastName: {
    type: Sequelize.TEXT,
    allowNull: false,
    unique: false,
    validate: {
      len: [4, 200],
    },
  },
  refreshToken: {
    type: Sequelize.TEXT,
  },
});
